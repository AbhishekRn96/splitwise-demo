import { useState } from 'react';
import Input from '../Input/Input';
import './SideSection.css'

export default function SideSection(props) {
  const [showInput, setShowInput] = useState(false);
  const [friendName, setFriendName] = useState('');

  const toggleShowInput = () => {
    setShowInput((curr) => curr = !curr);
  }

  const setName = (value) => {
    setFriendName(value.trim());
  }

  const addFriend = (friend) => {
    if(friend.trim() !== '') {
      let found = props.friendList.find((x) => x.name.toLowerCase() === friend.trim().toLowerCase())
      if(!found) {
        let friends = localStorage.getItem('myFriends');
        if(friends) {
          friends = JSON.parse(friends);
          friends.push({
            name : friend.trim(),
            gives : 0,
            gets : 0
          });
          localStorage.setItem('myFriends', JSON.stringify(friends));
        } else {
          friends = [{
            name : friend.trim(),
            gives : 0,
            gets : 0
          }];
          localStorage.setItem('myFriends', JSON.stringify(friends));
        }
        props.setFriendList([...friends]);
      }
      setName('');
    }
    toggleShowInput();
  }

  const managePageMode = (value) => {
    props.setPageMode(value);
  }

  return (
      <div className="card-body">
        <div className={props.pageMode === 'Dashboard' ? 'selectedSecMode mt-3 rounded-pill' : 'sideSecMode mt-3'} onClick={() => managePageMode('Dashboard')} role="button"><i className='bx bx-id-card'></i> <span>Dashboard</span></div>
        <div className={props.pageMode === 'All Expenses' ? 'selectedSecMode mt-3 rounded-pill' : 'sideSecMode mt-3'} onClick={() => managePageMode('All Expenses')} role="button"><i className='bx bx-list-ul'></i> <span>All Expenses</span></div>
        {/* <div className="mt-3 row justify-content-between ml-0 mr-0">
          <h5>Groups</h5>
          <div role="button"><i className='bx bx-plus-circle bx-sm'></i></div>
        </div> */}
        <div className="mt-3 row justify-content-between ml-0 mr-0">
          <h5>Friends</h5>
          <div onClick={toggleShowInput} role="button">{showInput ? <i className='bx bx-minus-circle bx-sm'></i> : <i className='bx bx-plus-circle bx-sm'></i>}</div>
        </div>
        {showInput ? <div className="row ml-0 mr-0 mt-2 justify-content-between">
          <Input
            type="text"
            inputClass="form-control friendInput"
            placeholder="Enter Name"
            onChange={(event) => setName(event.target.value)}
          />
          <div onClick={() => addFriend(friendName)} role="button" title="Add Friend"><i className='bx bxs-label bx-sm'></i></div>
        </div> : null}
        {props.friendList.length !== 0 ? props.friendList.map((data, key) => <div key={key} className="mt-2">
          <i className='bx bx-user-circle'></i> <span>{data.name}</span>
        </div>) : <p className="text-center mt-3">Click on the <i className='bx bx-plus-circle bx-sm pl-1 pr-1'></i> sign to add friends</p>}
      </div>
    )
}