import './AddExpense.css'
import { useState, useEffect } from 'react';
import Input from '../Input/Input';


export default function AddExpense(props) {
  const [findFriendName, setFindFriendName] = useState('');
  const [findFriendList, setFindFriendList] = useState([]);
  const [selected, setSelected] = useState([]);
  const [splitSelected, setSplitSelected] = useState([]);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [split, setSplit] = useState(0);
  const [selectPayer, setSelectPayer] = useState(false);
  const [selectSplit, setSelectSplit] = useState(false);
  const [payer, setPayer] = useState('you');
  const [expense, setExpense] = useState(0);
  const [description, setDescription] = useState('');
  const [addInfo, setAddInfo] = useState('');

  const addFriend = (friend) => {
    if(friend.trim() !== '') {
      let found = props.friendList.find((x) => x.name.toLowerCase() === friend.trim().toLowerCase())
      if(!found) {
        let friends = localStorage.getItem('myFriends');
        if(friends) {
          friends = JSON.parse(friends);
          friends.push({
            name : friend.trim(),
            gives : 0,
            gets : 0
          });

          localStorage.setItem('myFriends', JSON.stringify(friends));
        } else {
          friends = [{
            name : friend.trim(),
            gives : 0,
            gets : 0
          }];
          localStorage.setItem('myFriends', JSON.stringify(friends));
        }
        props.setFriendList([...friends]);
      }
    }
  }

  const findFriend = (value) => {
    if(value.trim() !== '') {
      setFindFriendName(value.trim());
      let arr = props.friendList.filter((x) => x.name.toLowerCase().includes(value.trim().toLowerCase()) === true)
      setFindFriendList(arr);
    } else if(value === ''){
      setFindFriendName('');
    }
  }

  const handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      let found = props.friendList.find((x) => x.name.toLowerCase() === findFriendName.trim().toLowerCase())
      if(!found)
        addFriend(findFriendName);
      let foundSeleted = selected.find((x) => x.toLowerCase() === findFriendName.trim().toLowerCase())
      if(!foundSeleted) {
        setSelected([...selected, findFriendName]);
      }
      setFindFriendName('');
      let arr = props.friendList.filter((x) => selected.indexOf(x.name) === -1)
      setFindFriendList(arr)
    }
  }

  const handleFocusBlur = (mode) => {
    if(mode === "focus") {
      let arr = props.friendList.filter((x) => selected.indexOf(x.name) === -1)
      setFindFriendList(arr)
      setShowSuggestions(true);
    }
  }

  const removeSelected = (key) => {
    let arr = selected;
    arr.splice(key, 1);
    setSelected([...arr]);
    setPayer('you');
  }

  const addSelected = (name) => {
    let foundSeleted = selected.find((x) => x.toLowerCase() === name.trim().toLowerCase())
    if(!foundSeleted)
      setSelected([...selected, name]);
  }

  const choosePayer = (name) => {
    setPayer(name);
    setSelectPayer(false);
  }

  const calculateSplit = (value , res = []) => {
    if(value !== '') {
      let filtered = res.length === 0 ? splitSelected.filter((x) => x.checked === true) : res.filter((x) => x.checked === true);
      if(filtered.length === 1 && filtered[0].name === 'You' && payer === 'you') {
        filtered = [];
        props.generateAlert("alert alert-danger", "Please add someone else to split the payment with")
      }
      if(filtered.length === 0) {
        setSplit(0);
        setExpense(0)
      } else {
        let final = Number(value) / (filtered.length);
        setSplit(final.toFixed(2))
      }
    } else {
      setSplit(0)
    }
  }

  const processSelected = (key, event) => {
    let arr = splitSelected;
    arr[key].checked = event.target.checked;
    setSplitSelected([...arr]);
    calculateSplit(expense);
  }

  const addExpense = () => {
    if(description.trim() !== '' && expense > 0) {
      let expenses = localStorage.getItem('myExpenses');
      let arr = splitSelected.filter((x) => x.checked === true);
      let newExpense = {
        description: description.trim(),
        addInfo: addInfo.trim(),
        amount: expense,
        split: split,
        paidBy: payer,
        paidFor : arr,
        date: new Date()
      }
      if(expenses) {
        expenses = JSON.parse(expenses);
        expenses.push(newExpense)
        localStorage.setItem('myExpenses', JSON.stringify(expenses));
        props.setAddedExpenses([...expenses.reverse()]);
      } else {
        expenses = [newExpense]
        localStorage.setItem('myExpenses', JSON.stringify(expenses));
        props.setAddedExpenses([...expenses]);
      }
      calculateBalance(newExpense);
    } else {
      props.generateAlert("alert alert-danger", "Please fill up all the mandatory fields")
    }
  }

  const calculateBalance = (newExpense) => {
    let friends = props.friendList;
    let paidFor = newExpense.paidFor;
    let amount = Number(newExpense.split)
    if(newExpense.paidBy !== 'you' && paidFor.findIndex((x) => x.name === 'You') !== -1) {
      let index = friends.findIndex((x) => x.name === newExpense.paidBy)
      if(index !== -1) {
        if(friends[index].gives === 0)
          friends[index].gets += amount;
        else {
          if(friends[index].gives <= amount) {
            friends[index].gets = amount - friends[index].gives;
            friends[index].gives = 0;
          } else {
            friends[index].gives = friends[index].gives - amount;
          }
        }
      }
    } else if(newExpense.paidBy === 'you') {
      for(let i = 0; i < paidFor.length; i++) {
        let index = friends.findIndex((x) => x.name === paidFor[i].name);
        if(index !== -1) {
          if(friends[index].gets === 0)
            friends[index].gives += amount;
          else {
            if(friends[index].gets <= amount) {
              friends[index].gives = amount - friends[index].gets;
              friends[index].gets = 0;
            } else {
              friends[index].gets = friends[index].gets - amount;
            }
          }
        }
      }
    }
    props.setOwe(friends.reduce((sum, a) => sum + Number(a.gets), 0));
    props.setOwed(friends.reduce((sum, a) => sum + Number(a.gives), 0));
    localStorage.setItem('myFriends', JSON.stringify(friends));
    props.setFriendList([...friends]);
    closeExpenseGenerator();
    props.generateAlert("alert alert-success", "Expense has been successfully added.")
  }

  const closeExpenseGenerator = () => {
    setSelectPayer(false);
    setSelectSplit(false);
    setSplitSelected([{name: "You", checked: true}]);
    setExpense(0);
    setSplit(0);
    setDescription('');
    setAddInfo('');
    setPayer('you');
    setSelected([]);
    setFindFriendList(props.friendList);
    setFindFriendName('');
  }

  const manageSplitSelected = (arr, calculateExpense = false) => {
    if(expense > 0 && calculateExpense) {
      calculateSplit(expense, arr);
    }
    setSplitSelected([...arr]);
  }

  useEffect(() => {
    if(props.friendList.length === 0) {
      let friends = localStorage.getItem('myFriends');
      if(friends) {
        friends = JSON.parse(friends);
        props.setOwe(friends.reduce((sum, a) => sum + Number(a.gets), 0));
        props.setOwed(friends.reduce((sum, a) => sum + Number(a.gives), 0));
        props.setFriendList([...friends]);
      }
    }
    let arr = props.friendList.filter((x) => selected.indexOf(x.name) === -1)
    setFindFriendList(arr);
    if(selected.length === 0) {
      setSelectPayer(false);
      setSelectSplit(false);
      manageSplitSelected([{name: "You", checked: true}])
      setExpense(0);
      setSplit(0);
    } else {
      let res = [{name: "You", checked: true}];
      for(let i = 0; i < selected.length; i++) {
        res.push({
          name: selected[i],
          checked: true
        })
      }
      manageSplitSelected(res, true)
    }
  }, [props, selected]);

  return(
    <div className="modal fade" id="addExpense" tabIndex="-1" role="dialog" aria-labelledby="addExpenseLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Add an Expense</h5>
              <button type="button" className="close" onClick={closeExpenseGenerator} data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
             <div className="position-relative">
               <span className="float-left">With <strong>you</strong> and:</span>
               <div className="row nameHolderBox">
                {selected.map((data, key) => <div key={key} className="rounded-pill ml-2 mb-2 nameHolder">{data}
                  <i className='bx bx-x ml-1 removeSelectedSuggestions' onClick={() => removeSelected(key)}></i></div>)}
                  <Input 
                    type="text"
                    inputClass="addExpenseFriend ml-2"
                    placeholder="Enter Name..."
                    value={findFriendName}
                    onChange={(event) => findFriend(event.target.value)} 
                    onKeyPress={handleKeyPress}
                    onFocus={() => handleFocusBlur("focus")}
                  />
               </div>
               {showSuggestions ? <div className="card friendSuggestion">
               <i className='bx bx-x bx-sm closeSuggestions' onClick={() => setShowSuggestions(false)}></i>
                {findFriendList.length > 0 ? findFriendList.map((data, key) => <div key={key} className="pt-2 pl-2 suggetionItem" onClick={() => addSelected(data.name)}>
                    {data.name}
                  </div>
                ) : <div className="pt-2 pl-2">Hit Enter to add friend</div>}
               </div> : null}
              </div><br />
              <div>
                <span className="text-danger">*</span>
                <Input 
                  type="text"
                  inputClass="expenseBody"
                  value={description} 
                  placeholder="Enter a description" 
                  onChange={(event) => setDescription(event.target.value)}
                /><br />
                <span className="text-danger">*</span>
                <Input 
                  type="number"
                  min="1" 
                  inputClass="expenseBody"
                  value={expense}
                  placeholder="&#8377;0"
                  onChange={(event) => {calculateSplit(event.target.value);setExpense(event.target.value);}}
                />
                <Input 
                  type="text" 
                  inputClass="expenseBody" 
                  placeholder="Additional information" 
                  value={addInfo} 
                  onChange={(event) => setAddInfo(event.target.value)}
                />
              </div>
              <div className="text-center mt-5">Paid by <span className="paidBy" onClick={() => {setSelectPayer(true);setSelectSplit(false);}}>{payer}</span> and split <span className="paidBy" onClick={() => {setSelectPayer(false);setSelectSplit(true);}}>equally​</span>.(₹{split}/person)</div>
              {selectPayer ? <div className="card outerCard">
                <div className="sideCardHead"><h5>Select Payer</h5></div>
                <i className='bx bx-x bx-sm closeSuggestions' onClick={() => {setSelectPayer(false);setSelectSplit(false);}}></i>
                <div className="pl-3 pt-2 pb-2 suggetionItem" onClick={() => choosePayer('you')}>You</div>
                {selected.map((data, key) => <div key={key} className="pl-3 pt-2 pb-2 suggetionItem" onClick={() => choosePayer(data)}>{data}</div>)}
              </div> : null}
              {selectSplit ? <div className="card outerCard">
                <div className="sideCardHead"><h5>Choose Split</h5></div>
                <i className='bx bx-x bx-sm closeSuggestions' onClick={() => {setSelectPayer(false);setSelectSplit(false);}}></i>
                {splitSelected.map((data, key) => <div key={key} className="ml-3 pt-2 pb-2 form-check">
                  <Input 
                    type="checkbox" 
                    checked={data.checked}
                    onChange={(event) => processSelected(key, event)}
                    inputClass="form-check-input"
                    id={data.name}
                    label={true}
                    htmlFor={data.name}
                    labelText={data.name}
                    labelClass="form-check-label"
                  />
                </div>)}
              </div> : null}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn addExpense" disabled={description.trim() !== '' && expense > 0 ? false : true} onClick={addExpense} data-dismiss="modal">Add Expense</button>
              <button type="button" className="btn btn-secondary" onClick={closeExpenseGenerator} data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
  )
}