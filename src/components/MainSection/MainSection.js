import './MainSection.css';
import { useState, useEffect } from 'react';
import Alert from '../Alert/Alert';
import SideSection from '../SideSection/SideSection';
import Dashboard from '../Dashboard/Dashboard';
import AddExpense from '../AddExpense/AddExpense';
import AllExpenses from '../AllExpenses/AllExpenses';

export default function MainSection() {

  const [friendList, setFriendList] = useState([]);
  const [alertBody, setAlertBody] = useState(null);
  const [owe, setOwe] = useState(0);
  const [owed, setOwed] = useState(0);
  const [pageMode, setPageMode] = useState('Dashboard');
  const [addedExpenses, setAddedExpenses] = useState([]);

  const generateAlert = (className, message) => {
    setAlertBody({
      alertClass: className+" alertHolder",
      message: message
    })
    setTimeout(() => setAlertBody(null), 3000)
  }

  useEffect(() => {
    if(friendList.length === 0) {
      let friends = localStorage.getItem('myFriends');
      let myExpenses = localStorage.getItem('myExpenses');
      if(friends) {
        friends = JSON.parse(friends);
        setOwe(friends.reduce((sum, a) => sum + Number(a.gets), 0));
        setOwed(friends.reduce((sum, a) => sum + Number(a.gives), 0));
        setFriendList([...friends]);
      }
      if(myExpenses) {
        myExpenses = JSON.parse(myExpenses);
        setAddedExpenses([...myExpenses.reverse()])
      }
    }
  });

  return (
    <div>
      <div className="position-relative">
        {alertBody ? <Alert 
          alertClass={alertBody.alertClass}
          message={alertBody.message}
        /> : null}
      </div>
      <div className="row ml-0 mr-0 justify-content-between">
      <div className="card left-card">
        <SideSection 
          friendList={friendList} 
          setFriendList={setFriendList}
          pageMode={pageMode} 
          setPageMode={setPageMode}
        />
      </div>
        <div className="card right-card">
          <div>
            <h4 className="float-left mt-3 ml-3">{pageMode}</h4>
            <button className="btn addExpense rounded-pill float-right mt-3 mr-3"
              data-toggle="modal" data-target="#addExpense">Add an Expense</button>
          </div>
          <div className="row mt-3 ml-0 mr-0 position-relative">
            <div className="oweBoxes">you owe <br /><span className="moneyLent">&#8377;{owe}</span></div>
            <div className="divider"></div>
            <div className="oweBoxes">you are owed <br /><span className="money">&#8377;{owed}</span></div>
          </div>
          <div className="card-body overflow-auto">
            {pageMode === 'Dashboard' ? <Dashboard 
              friendList={friendList}
            /> : pageMode === 'All Expenses' ? <AllExpenses 
              addedExpenses={addedExpenses}
            /> : null}
          </div>
        </div>
      </div>
      <AddExpense 
        friendList={friendList} 
        setFriendList={setFriendList}
        setAlertBody={setAlertBody}
        setOwe={setOwe}
        setOwed={setOwed}
        setAddedExpenses={setAddedExpenses}
        generateAlert={generateAlert}
      />
    </div>
  )
}