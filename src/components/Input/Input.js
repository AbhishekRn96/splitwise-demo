import { Fragment } from 'react';
import './Input.css';

export default function Input(props) {
  return(<Fragment>
    {props.label && props.type !== 'checkbox' ? <label htmlFor={props.for} className={props.labelClass}>{props.labelText}</label> : null}
    <input
      type={props.type}
      value={props.value}
      className={props.inputClass}
      placeholder={props.placeholder}
      onChange={props.onChange}
      onKeyPress={props.onKeyPress}
      onFocus={props.onFocus}
      min={props.min}
      checked={props.checked}
      id={props.id}
    />
    {props.label && props.type === 'checkbox' ? <label htmlFor={props.for} className={props.labelClass}>{props.labelText}</label> : null}
    </Fragment>
  )
}