import './Dashboard.css'

export default function Dashboard(props) {
  return (
    <div className="row ml-0 mr-0">
      <div className="oweContent">
        {props.friendList.map((data, key) => data.gets !== 0 ?
          <div key={key} className="pl-3 mt-3" >
            <div>{data.name}</div>
            <div className="moneyLent font-14">you owe <b className="moneyLent font-16">&#8377;{data.gets}</b></div>
          </div>
        : null)}
      </div>
      <div className="oweContent">
        {props.friendList.map((data, key) => data.gives !== 0 ?
          <div key={key} className="pl-3 mt-3" >
            <div>{data.name}</div>
            <div className="money font-14">owes you <b className="money font-16">&#8377;{data.gives}</b></div>
          </div>
        : null)}
      </div>
    </div>
  )
}