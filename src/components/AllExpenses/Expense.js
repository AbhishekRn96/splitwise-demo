import './AllExpenses.css';

const monthNames = ["January", "February", "March", "April", "May", "June",
"July", "August", "September", "October", "November", "December"
];

export default function Expense(props) {

  const generateDate = (data) => {
    let date = new Date(data.date)
    return monthNames[date.getMonth()]+' '+date.getDate()
  }

  const getLentStatus = (data) => {
    let index = data.paidFor.findIndex((x) => x.name === 'You')
    if(index > -1 && data.paidBy === 'you') {
      return(
        <div className="moneyPart">
          <div className="font-14 text-capitalize">You lent</div>
          <div className="font-16 money">&#8377;{(Number(data.amount)-Number(data.split)).toFixed(2)}</div>
        </div>
      )
    } else if(index === -1 && data.paidBy === 'you') {
      return(
        <div className="moneyPart">
          <div className="font-14 text-capitalize">You lent</div>
          <div className="font-16 money">&#8377;{data.amount}</div>
        </div>
      )
    } else if(index > -1 && data.paidBy !== 'you') {
      return(
        <div className="moneyPart">
          <div className="font-14 text-capitalize">{data.paidBy} lent</div>
          <div className="font-16 moneyLent">&#8377;{Number(data.split).toFixed(2)}</div>
        </div>
      )
    } else if(index === -1 && data.paidBy !== 'you') {
      return(
        <div className="moneyPart">
          <div className="font-14">Not involved</div>
        </div>
      )
    }
  }

  return (
    <div className="row ml-0 mr-0 mt-2 expense">
      <div className="descPart">
        <div>{props.data.description}</div>
        <div className="font-14">{generateDate(props.data)}</div>
      </div>  
      <div className="moneyPart">
        <div className="font-14 text-capitalize">{props.data.paidBy} paid</div>
        <div className={props.data.paidBy === 'you' ? 'font-16 money' : 'font-16 moneyLent'}>&#8377;{props.data.amount}</div>
      </div>
      {getLentStatus(props.data)}
    </div>
  )
}