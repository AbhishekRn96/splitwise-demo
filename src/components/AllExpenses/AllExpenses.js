import Expense from './Expense';

export default function AllExpenses(props) {

  return (
    <div>
      {props.addedExpenses.length !== 0 ? props.addedExpenses.map((data, key) =><div key={key}>
        <Expense 
          data={data}
        />  
      </div>) : <div className="text-center mt-4">Click on <button className="btn addExpense rounded-pill">Add an Expense</button> above to add an expense</div>}
    </div>
  )
}