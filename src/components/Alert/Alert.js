export default function Alert(props) {

  return(
    <div className={props.alertClass} role="alert">
      {props.message}
    </div>
  )
}