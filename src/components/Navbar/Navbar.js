import './Navbar.css';
import { ThemeContext } from '../../context';
import { useContext } from 'react';

export default function Navbar(props) {

  const theme = useContext(ThemeContext);

  const toggleTheme = () => {
    props.toggle()
  } 

  return (
    <nav className="navbar navBarHeight navShadow">
      <div className="container">
        <h3 className="splitWise">SplitWise</h3>
        <div className="navbar-nav ml-auto">
          <li className="nav-item">
            {theme === 'light' ? <div className="nav-link">
                <i className='bx bxs-toggle-left toggle bx-md' title="Click Me" onClick={toggleTheme}></i>
                <i className='bx bxs-sun sun bx-md ml-2' title='Day'></i> 
              </div> : <div className="nav-link">
                <i className='bx bxs-toggle-right toggle bx-md' title="Click Me" onClick={toggleTheme}></i> 
                <i className='bx bxs-moon moon bx-md ml-2' title='Night'></i>
              </div>}
          </li>
        </div>
      </div>
    </nav>
  )
}