import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/boxicons/css/boxicons.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap';
import '../node_modules/jquery/dist/jquery.slim';
import { useState } from 'react';
import Navbar from './components/Navbar/Navbar';
import MainSection from './components/MainSection/MainSection';
import { ThemeContext } from './context';


function App() {

  const [theme, setTheme] = useState("light");

  const toggleTheme = () => {
    setTheme((current) => current === "light" ? "dark" : "light");
  }

  return (
    <ThemeContext.Provider value={theme}>
      <div className="App" data-theme={theme}>
        <Navbar toggle={toggleTheme} />
        <div className="nonNavBody container">
          <MainSection />
        </div>
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
